# Hylke Middel

My strong points:

* Large knowledge of genetics
* Statistical programming (in R)
* Knowledge of machine learning

I have always had great interest in computers and genetics, and have shown to be able to put this passion to good use with the various projects I have worked on in the last couple of years. Some good examples of this can be found in the references below.

## References
### Machine learning
[Machine Learning using weka] https://bitbucket.org/hmiddel/machine-learning-p53/src/master/

### Statistical programming
[RNA-seq data processing] Can be found in this repository

### Modeling
[The circadian clock: a computational model] https://bitbucket.org/wendy1998/thema08-systemsbiology/src/master/